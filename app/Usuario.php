<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //
    protected $table = "usuario";
    protected $fillable = ['nombre',
						    'apellidopat',
						    'apellidomat',
						    'email',
						    'fchnac',
						    'fchingreso',
						    'avatar',
						    'created_at'
						];

    
}
