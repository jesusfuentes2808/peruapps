<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Carbon\Carbon;
use DB;
use App\Http\Repositories\Usuarios;

class UsuarioController extends Controller
{
    //
    protected $usuarioRepo;

    public function __construct(Usuarios $usuariosRepo){
        $this->usuariosRepo = $usuariosRepo;
    }


    public function create(Request $request)
    {
    	$usuario = new Usuario();
    	$usuarioVal = new Usuario();
    	$numeroUsuario = $usuarioVal->where('email',$request->input('email'))->count();

    	if($numeroUsuario>0)
    		return response()->json(['mensaje'=>'El email ya está registrado en la base de datos']);

    	$usuario->nombre = $request->input('nombre');
	    $usuario->apellidopat = $request->input('apellidopat');
	    $usuario->apellidomat = $request->input('apellidomat');
	    $usuario->email = $request->input('email');
	    $usuario->fchnac = $request->input('fchnac');
	    $usuario->fchingreso = Carbon::now()->toDateTimeString();

	    $usuario->save();

	    return response()->json($usuario);
    }

    public function update($id,Request $request)
    {
    	$usuario = $this->usuariosRepo->update($id,$request);
    	
    	return response()->json($usuario);
    }

    public function get(Request $request)
    {
    	$perpage = 3;
    	$pagina  = intval($request->page);
    	$inicio  = ($pagina-1)*$perpage;
    	
    	$data = $this->usuariosRepo->get($perpage,$pagina,$inicio);

    	return response()->json($data);
    }

    public function detail($id)
    {
    	$usuario = $this->usuariosRepo->detail($id);

		return response()->json($usuario);
    }
}
