<?php

namespace App\Http\Repositories;

use DB;
use App\Usuario;

class Usuarios{

	public function get($perpage,$pagina,$inicio){
		$usuario = DB::table('usuario')->select(DB::raw('id,nombre as first_name,apellidomat as last_name,avatar'))->offset($inicio)->limit(3)->get();
		$usuarioCount = Usuario::count();

		$data['page'] = $pagina;
		$data['per_page'] = $perpage;
		$data['total'] = $usuarioCount;
		$data['total_pages'] = ceil($usuarioCount/3);
		$data['data'] = $usuario;

		return $data;

	}	

	public function update($id,$request)
    {
    	$usuario = Usuario::find($id);
		    	
    	$usuario->nombre = $request->input('nombre');
	    $usuario->apellidopat = $request->input('apellidopat');
	    $usuario->apellidomat = $request->input('apellidomat');
	    $usuario->email = $request->input('email');
	    $usuario->fchnac = $request->input('fchnac');
	    $usuario->fchingreso = Carbon::now()->toDateTimeString();

	    $usuario->save();
    	
    	return $usuario;
    }

    public function detail($id)
    {
    	$usuario = Usuario::find($id);

		return $usuario;
    }
}

